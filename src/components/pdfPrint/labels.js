//9种考勤状态
export const labels=[
    {
        title:"迟到",
        key:"chidao",
        bgColor:"#2db7f5"
    },
    {
        title:"早退",
        key:"zaotui",
        bgColor:"#ed4014"
    },
    {
        title:"缺勤",
        key:"queqin",
        bgColor:"#ff9900"
    },
    // {
    //     title:"违纪",
    //     key:"weiji",
    //     bgColor:"#FF00CC"
    // },
    // {
    //     title:"未穿校服",
    //     key:"xiaofu",
    //     bgColor:"#66FFFF"
    // },
    {
        title:"病假",
        key:"bingjia",
        bgColor:"#19be6b"
    },
    {
        title:"事假",
        key:"shijia",
        bgColor:"#17233d"
    },
    // {
    //     title:"仪容仪表（发型）",
    //     key:"faxing",
    //     bgColor:"#FFCCCC"
    // },
    // {
    //     title:"仪容仪表（化妆）",
    //     key:"huazhuang",
    //     bgColor:"#FF9900"
    // },
]
